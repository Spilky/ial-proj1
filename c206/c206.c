	
/* c206.c **********************************************************}
{* T�ma: Dvousm�rn� v�zan� line�rn� seznam
**
**                   N�vrh a referen�n� implementace: Bohuslav K�ena, ��jen 2001
**                            P�epracovan� do jazyka C: Martin Tu�ek, ��jen 2004
**                                             �pravy: Bohuslav K�ena, ��jen 2013
**
** Implementujte abstraktn� datov� typ dvousm�rn� v�zan� line�rn� seznam.
** U�ite�n�m obsahem prvku seznamu je hodnota typu int.
** Seznam bude jako datov� abstrakce reprezentov�n prom�nnou
** typu tDLList (DL znamen� Double-Linked a slou�� pro odli�en�
** jmen konstant, typ� a funkc� od jmen u jednosm�rn� v�zan�ho line�rn�ho
** seznamu). Definici konstant a typ� naleznete v hlavi�kov�m souboru c206.h.
**
** Va��m �kolem je implementovat n�sleduj�c� operace, kter� spolu
** s v��e uvedenou datovou ��st� abstrakce tvo�� abstraktn� datov� typ
** obousm�rn� v�zan� line�rn� seznam:
**
**      DLInitList ...... inicializace seznamu p�ed prvn�m pou�it�m,
**      DLDisposeList ... zru�en� v�ech prvk� seznamu,
**      DLInsertFirst ... vlo�en� prvku na za��tek seznamu,
**      DLInsertLast .... vlo�en� prvku na konec seznamu, 
**      DLFirst ......... nastaven� aktivity na prvn� prvek,
**      DLLast .......... nastaven� aktivity na posledn� prvek, 
**      DLCopyFirst ..... vrac� hodnotu prvn�ho prvku,
**      DLCopyLast ...... vrac� hodnotu posledn�ho prvku, 
**      DLDeleteFirst ... zru�� prvn� prvek seznamu,
**      DLDeleteLast .... zru�� posledn� prvek seznamu, 
**      DLPostDelete .... ru�� prvek za aktivn�m prvkem,
**      DLPreDelete ..... ru�� prvek p�ed aktivn�m prvkem, 
**      DLPostInsert .... vlo�� nov� prvek za aktivn� prvek seznamu,
**      DLPreInsert ..... vlo�� nov� prvek p�ed aktivn� prvek seznamu,
**      DLCopy .......... vrac� hodnotu aktivn�ho prvku,
**      DLActualize ..... p�ep��e obsah aktivn�ho prvku novou hodnotou,
**      DLSucc .......... posune aktivitu na dal�� prvek seznamu,
**      DLPred .......... posune aktivitu na p�edchoz� prvek seznamu, 
**      DLActive ........ zji��uje aktivitu seznamu.
**
** P�i implementaci jednotliv�ch funkc� nevolejte ��dnou z funkc�
** implementovan�ch v r�mci tohoto p��kladu, nen�-li u funkce
** explicitn� uvedeno n�co jin�ho.
**
** Nemus�te o�et�ovat situaci, kdy m�sto leg�ln�ho ukazatele na seznam 
** p�ed� n�kdo jako parametr hodnotu NULL.
**
** Svou implementaci vhodn� komentujte!
**
** Terminologick� pozn�mka: Jazyk C nepou��v� pojem procedura.
** Proto zde pou��v�me pojem funkce i pro operace, kter� by byly
** v algoritmick�m jazyce Pascalovsk�ho typu implemenov�ny jako
** procedury (v jazyce C procedur�m odpov�daj� funkce vracej�c� typ void).
**/

#include "c206.h"

int solved;
int errflg;

void DLError() {
/*
** Vytiskne upozorn�n� na to, �e do�lo k chyb�.
** Tato funkce bude vol�na z n�kter�ch d�le implementovan�ch operac�.
**/	
    printf ("*ERROR* This program has performed an illegal operation.\n");
    errflg = TRUE;             /* glob�ln� prom�nn� -- p��znak o�et�en� chyby */
    return;
}

void DLInitList (tDLList *L)	{
/*
** Provede inicializaci seznamu L p�ed jeho prvn�m pou�it�m (tzn. ��dn�
** z n�sleduj�c�ch funkc� nebude vol�na nad neinicializovan�m seznamem).
** Tato inicializace se nikdy nebude prov�d�t nad ji� inicializovan�m
** seznamem, a proto tuto mo�nost neo�et�ujte. V�dy p�edpokl�dejte,
** �e neinicializovan� prom�nn� maj� nedefinovanou hodnotu.
**/
    
	
 L->First = NULL;       //vynulov�n� ukazatele na prvn� prvek
 L->Act = NULL;         //vynulov�n� ukazatele na aktivn� prvek
 L->Last = NULL;        //vynulov�n� ukazatele na posledn� prvek
}

void DLDisposeList (tDLList *L)	{
/*
** Zru�� v�echny prvky seznamu L a uvede seznam do stavu, v jak�m
** se nach�zel po inicializaci. Ru�en� prvky seznamu budou korektn�
** uvoln�ny vol�n�m operace free. 
**/
	
    tDLElemPtr tmp = NULL;              //pomocn� ukazatel

    while(L->Last != NULL) {            //dokud nebude seznam pr�zdn�
        tmp = L->Last;                  //pomocn� ukazatel ukazuje na posledn� prvek seznamu
        L->Last = L->Last->lptr;        //posledn� prvek ukazuje na p�edposledn�
        free(tmp);                      //uvoln�n� pam�ti zab�ran� p�vodn�m posledn�m prvkem
    }

    L->Act = NULL;                      //vynulov�n� ukazatele na aktivn� prvek
    L->First = NULL;                    //vynulov�n� ukazatele na prvn� prvek
}

void DLInsertFirst (tDLList *L, int val)	{
/*
** Vlo�� nov� prvek na za��tek seznamu L.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
    tDLElemPtr tmp = NULL;              //pomocn� ukazatel
    if((tmp = malloc(sizeof(struct tDLElem))) != NULL) {        //pokud se povede naalokovat pam� pro nov� prvek
        tmp->data = val;                            //vlo��m data
        tmp->lptr = NULL;                           //vynuluju ukazatel na p�edchoz� prvek
        tmp->rptr = NULL;                           //vynuluju ukazatel na dal�� prvek

        if(L->First == NULL) {                      //pokud je seznam pr�zdn�
            L->Last = tmp;                          //ukazatel na posledn� prvek se rovn� ukazateli na nov� prvek
        }
        else {                                      //jinak
            tmp->rptr = L->First;                   //ukazatel na dal�� prvek se rovn� p�vodn�mu prvn�mu
            L->First->lptr = tmp;                   //ukazatel na p�edchoz� prvek p�vodn�ho prvn�ho prvku se rovn� ukazateli na vkl�dan� prvek
        }
        L->First = tmp;                             //ukazatel na prvn� prvek se rovn� ukazateli na vkl�dan� prvek
    }
    else {                                          //pokud se nepoda�� alokovat pam�
        DLError();                                  //vypi� chybu
    }
    return;
}

void DLInsertLast(tDLList *L, int val)	{
/*
** Vlo�� nov� prvek na konec seznamu L (symetrick� operace k DLInsertFirst).
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/ 	
    tDLElemPtr tmp = NULL;                          //pomocn� ukazatel
    if((tmp = malloc(sizeof(struct tDLElem))) != NULL) {        //pokud se povede naalokovat pam� pro nov� prvek
        tmp->data = val;                            //vlo��m data
        tmp->lptr = NULL;                           //vynuluju ukazatel na p�edchoz� prvek
        tmp->rptr = NULL;                           //vynuluju ukazatel na dal�� prvek

        if(L->First == NULL) {                      //pokud je seznam pr�zdn�
            L->First = tmp;                         //ukazatel na prvn� prvek se rovna ukazateli na nov� prvek
        }
        else {                                      //jinak
            tmp->lptr = L->Last;                    //ukazatel na p�edchoz� prvek se rovn� p�vodn�mu posledn�mu
            L->Last->rptr = tmp;                    //ukazatel na n�slednuj�c� prvek p�vodn�ho posledn�ho prvku se rovn� ukazateli na vkl�dan� prvek
        }
        L->Last = tmp;                              //ukazatel na posledn� prvek se rovn� ukazateli na vkl�dan� prvek
    }
    else {                                          //pokud se nepoda�� alokovat pam�
        DLError();                                  //vyp��u chybu
    }
    return;
}

void DLFirst (tDLList *L)	{
/*
** Nastav� aktivitu na prvn� prvek seznamu L.
** Funkci implementujte jako jedin� p��kaz (nepo��t�me-li return),
** ani� byste testovali, zda je seznam L pr�zdn�.
**/
    L->Act = L->First;          //ukazatel na aktivn� prvek se rovn� ukazateli na prvn� prvek
    return;
}

void DLLast (tDLList *L)	{
/*
** Nastav� aktivitu na posledn� prvek seznamu L.
** Funkci implementujte jako jedin� p��kaz (nepo��t�me-li return),
** ani� byste testovali, zda je seznam L pr�zdn�.
**/
    L->Act = L->Last;          //ukazatel na aktivn� prvek se rovn� ukazateli na posledn� prvek
    return;
}

void DLCopyFirst (tDLList *L, int *val)	{
/*
** Prost�ednictv�m parametru val vr�t� hodnotu prvn�ho prvku seznamu L.
** Pokud je seznam L pr�zdn�, vol� funkci DLError().
**/
    if(L->First != NULL) {              //pokud nen� seznam pr�czdn�
        *val = L->First->data;          //vlo��m do val data prvn�ho prvku
    }
    else {                              //jinak
        DLError();                      //vyp��u chybu
    }
    return;
}

void DLCopyLast (tDLList *L, int *val)	{
/*
** Prost�ednictv�m parametru val vr�t� hodnotu posledn�ho prvku seznamu L.
** Pokud je seznam L pr�zdn�, vol� funkci DLError().
**/
    if(L->Last != NULL) {               //pokud nen� seznam pr�zdn�
        *val = L->Last->data;           //vlo��m do val data posledn�ho prvku
    }
    else {                              //jinak
        DLError();                      //vyp��u chybu
    }
    return;
}

void DLDeleteFirst (tDLList *L)	{
/*
** Zru�� prvn� prvek seznamu L. Pokud byl prvn� prvek aktivn�, aktivita 
** se ztr�c�. Pokud byl seznam L pr�zdn�, nic se ned�je.
**/
    if(L->First != NULL) {              //pokud nen� seznam pr�zdn�
        if(L->Act == L->First) {        //pokud je prvn� prvek i aktivn�m prvkem
            L->Act = NULL;              //aktivita se ztr�c�
        }

        if(L->Last == L->First) {       //pokud je prvn� prvek i posledn�m prvkem
            L->Last = NULL;             //nuluju ukazatel na posledn�
        }

        tDLElemPtr tmp = NULL;          //pomocn� ukazatel
        tmp = L->First->rptr;           //pomocn� ukazatel bude ukazovat na n�sleduj�c� prvek po mazan�m
        free(L->First);                 //uvoln�m prvn� prvek
        L->First = tmp;                 //nastav�m nov� prvn� prvek

        if(L->First != NULL) {          //pokud nen� seznam pr�zdn�
            L->First->lptr = NULL;      //vynuluju ukazatel na p�edchoz� prvek nov�ho prvn�ho prvku
        }
    }
    return;
}	

void DLDeleteLast (tDLList *L)	{
/*
** Zru�� posledn� prvek seznamu L. Pokud byl posledn� prvek aktivn�,
** aktivita seznamu se ztr�c�. Pokud byl seznam L pr�zdn�, nic se ned�je.
**/ 
    if(L->First != NULL) {              //pokud nen� seznam pr�zdn�
        if(L->Act == L->Last) {        //pokud je posledn� prvek i aktivn�m prvkem
            L->Act = NULL;             //aktivita se ztr�c�
        }

        if(L->Last == L->First) {       //pokud je posledn� prvek i prvn�m prvkem
            L->First = NULL;            //nuluju ukazatel na posledn�
        }

        tDLElemPtr tmp = NULL;          //pomocn� ukazatel
        tmp = L->Last->lptr;            //pomocn� ukazatel bude ukazovat na p�edchoz� prvek p�ed mazan�m
        free(L->Last);                  //uvoln�m posledn� prvek
        L->Last = tmp;                  //nastav�m nov� posledn� prvek

        if(L->Last != NULL) {           //pokud nen� seznam pr�zdn�
            L->Last->rptr = NULL;       //vynuluju ukazatel na n�sleduj�c� prvek nov�ho posledn�ho prvku
        }
    }
    return;
}

void DLPostDelete (tDLList *L)	{
/*
** Zru�� prvek seznamu L za aktivn�m prvkem.
** Pokud je seznam L neaktivn� nebo pokud je aktivn� prvek
** posledn�m prvkem seznamu, nic se ned�je.
**/
    if((L->Act != L->Last) && (L->Act != NULL)) {       //pokud je seznam aktivn� nebo pokud nen� aktivn� prvek posledn�m prvekm

        tDLElemPtr tmp = NULL;                          //pomocn� ukazatel

        if(L->Act->rptr == L->Last) {                   //pokud je n�sleduj�c� prvek po aktivn�m posledn�m prvkem
            tmp = L->Last;                              //pomocn� ukazatel ukazuje na posledn�
            L->Act->rptr = NULL;                        //vynuluju ukazatel na n�sleduj�c� prvek u aktivn�ho prvku
            L->Last = L->Act;                           //posledn� prvek bude nyn� z�rove� i aktivn� prvek
            free(tmp);                                  //uvoln�m p�vodn� posledn�
        }
        else {                                          //jinak
            tmp = L->Act->rptr;                         //pomocn� ukazatel ukazuje na n�sleduj�c� po aktivn�m
            L->Act->rptr = tmp->rptr;                   //sv�u aktivn� prvek s prvkem n�sleduj�c�m po mazan�m
            tmp->rptr->lptr = L->Act;                   //sv�u n�sleduj�c� prvek po mazan�m s aktivn�m
            free(tmp);                                  //uvoln�m mazan� prvek
        }
    }
    return;
}

void DLPreDelete (tDLList *L)	{
/*
** Zru�� prvek p�ed aktivn�m prvkem seznamu L .
** Pokud je seznam L neaktivn� nebo pokud je aktivn� prvek
** prvn�m prvkem seznamu, nic se ned�je.
**/
    if(L->Act != L->First && L->Act != NULL) {      //pokud je seznam aktivn� nebo pokud nen� aktivn� prvek posledn�m prvekm

        tDLElemPtr tmp = NULL;                      //pomocn� ukazatel

        if(L->Act->lptr == L->First) {              //pokud je p�edchoz� prvek p�ed aktivn�m prvn�m prvkem
            tmp = L->First;                         //pomocn� ukazatel ukazuje na prvn�
            L->Act->lptr = NULL;                    //vynuluju ukazatel na p�edchoz� prvek u aktivn�ho prvku
            L->First = L->Act;                      //prvn� prvek bude nyn� z�rove� i aktivn� prvek
            free(tmp);                              //uvoln�m p�vodn� prvn�
        }
        else {                                      //jinak
            tmp = L->Act->lptr;                     //pomocn� ukazatel ukazuje na p�edchoz� p�ed aktivn�m
            L->Act->lptr = tmp->lptr;               //sv�u aktivn� prvek s prvkem p�edch�zej�c�m mazan�mu
            tmp->lptr->rptr = L->Act;               //sv�u p�edchoz� prvek p�ed mazan�m s aktivn�m
            free(tmp);                              //uvoln�m mazan� prvek
        }
    }
    return;
}

void DLPostInsert (tDLList *L, int val) {
/*
** Vlo�� prvek za aktivn� prvek seznamu L.
** Pokud nebyl seznam L aktivn�, nic se ned�je.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
    if(L->Act != NULL){                                         //pokud je seznam aktivn�
        tDLElemPtr tmp = NULL;                                  //pomocn� ukazatel
        if((tmp = malloc(sizeof(struct tDLElem))) != NULL) {    //pokud se povede naalokovat pam� pro nov� prvek
            tmp->data = val;                                    //vlo��m data nov�mu prvku
            tmp->lptr = tmp->rptr = NULL;                       //vynuluji jeho ukazatele

            if(L->Last == L->Act) {                             //pokud je aktivn� prvek tak� posledn�m prvkem
                L->Last = tmp;                                  //nastav�m ukazatel na posledn�
            }
            else {                                              //jinak
                L->Act->rptr->lptr = tmp;                       //sv�u n�sleduj�c� za vkl�dan�m s vkl�dan�m
                tmp ->rptr = L->Act->rptr;                      //sv�u vkl�dan� s n�sleduj�c�m
            }
            tmp->lptr = L->Act;                                 //sv�u vkl�dan� s aktivn�m
            L->Act->rptr = tmp;                                 //sv�u aktivn� s vkl�dan�m
        }
        else {                                                  //jinak
            DLError();                                          //vypi� chybu
        }
    }
    return;
}

void DLPreInsert (tDLList *L, int val)		{
/*
** Vlo�� prvek p�ed aktivn� prvek seznamu L.
** Pokud nebyl seznam L aktivn�, nic se ned�je.
** V p��pad�, �e nen� dostatek pam�ti pro nov� prvek p�i operaci malloc,
** vol� funkci DLError().
**/
    if(L->Act != NULL){                                         //pokud je seznam aktivn�
        tDLElemPtr tmp = NULL;                                  //pomocn� ukazatel
        if((tmp = malloc(sizeof(struct tDLElem))) != NULL) {    //pokud se povede naalokovat pam� pro nov� prvek
            tmp->data = val;                                    //vlo��m data nov�mu prvku
            tmp->lptr = tmp->rptr = NULL;                       //vynuluji jeho ukazatele

            if(L->First == L->Act) {                             //pokud je aktivn� prvek tak� prvn�m prvkem
                L->First = tmp;                                  //nastav�m ukazatel na prvn�
            }
            else {                                              //jinak
                L->Act->lptr->rptr = tmp;                       //sv�u p�edchoz� p�ed vkl�dan�m s vkl�dan�m
                tmp ->lptr = L->Act->lptr;                      //sv�u vkl�dan� s p�edchoz�m
            }
            tmp->rptr = L->Act;                                 //sv�u vkl�dan� s aktivn�m
            L->Act->lptr = tmp;                                 //sv�u aktivn� s vkl�dan�m
        }
        else {                                                  //jinak
            DLError();                                          //vypi� chybu
        }
    }
    return;
}

void DLCopy (tDLList *L, int *val)	{
/*
** Prost�ednictv�m parametru val vr�t� hodnotu aktivn�ho prvku seznamu L.
** Pokud seznam L nen� aktivn�, vol� funkci DLError ().
**/
    if(L->Act != NULL) {            //pokud je seznam aktivn�
        *val = L->Act->data;        //do val ulo� data aktivn�ho prvku
    }
    else {                          //jinak
        DLError();                  //vypi� chybu
    }
    return;
}

void DLActualize (tDLList *L, int val) {
/*
** P�ep��e obsah aktivn�ho prvku seznamu L.
** Pokud seznam L nen� aktivn�, ned�l� nic.
**/
    if(L->Act != NULL) {        //pokud je seznam aktivn�
        L->Act->data = val;     //data aktivn�ho prvku napl� hodnotou z val
    }
    return;
}

void DLSucc (tDLList *L)	{
/*
** Posune aktivitu na n�sleduj�c� prvek seznamu L.
** Nen�-li seznam aktivn�, ned�l� nic.
** V�imn�te si, �e p�i aktivit� na posledn�m prvku se seznam stane neaktivn�m.
**/
    if(L->Act != NULL) {            //pokud je seznam aktivn�
        L->Act = L->Act->rptr;      //ukazatel na aktivn� bude ukazovat na n�sleduj�c� po aktivn�m
    }
    return;
}


void DLPred (tDLList *L)	{
/*
** Posune aktivitu na p�edchoz� prvek seznamu L.
** Nen�-li seznam aktivn�, ned�l� nic.
** V�imn�te si, �e p�i aktivit� na prvn�m prvku se seznam stane neaktivn�m.
**/
    if(L->Act != NULL) {            //pokud je seznam aktivn�
        L->Act = L->Act->lptr;      //ukazatel na aktivn� bude ukazovat na p�edchoz� p�ed aktivn�m
    }
    return;
}

int DLActive (tDLList *L) {		
/*
** Je-li seznam aktivn�, vrac� true. V opa�n�m p��pad� vrac� false.
** Funkci implementujte jako jedin� p��kaz.
**/
    return (L->Act == NULL) ? 0 : 1;
}

/* Konec c206.c*/
